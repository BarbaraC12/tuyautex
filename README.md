# pipex

This project aims to make you understand a bit deeper two concets that you already know: the redirections and the pipes. It is an introductory project for the bigger UNIX projects that will appear later on in the cursus. 

Grade : 100 ✅

## Usage

```sh
make
./pipex file1 cmd1 cmd2 file2
```
