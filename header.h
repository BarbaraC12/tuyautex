#ifndef HEADER_H
# define HEADER_H
# define _GNU_SOURCE

# ifdef __linux
#  define STDIN 0
#  define STDOUT 1
#  define STDERR 2
# elif __APPLE__
#  define STDIN 0
#  define STDOUT 1
#  define STDERR 2
# endif

# include <errno.h>
# include <fcntl.h>
# include <stdarg.h>
# include <stddef.h>
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <sys/wait.h>
# include <time.h>
# include <unistd.h>
# include "functions.h"

#endif
