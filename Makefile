NAME		= pipex

SRCS		= srcs/main.c srcs/pipex.c srcs/lib.c srcs/get_env.c srcs/lib_split.c srcs/free_mem.c

OBJS_PATH	= ./objs/
TMP			= ${SRCS:.c=.o}
OBJS		= $(addprefix ${OBJS_PATH},${TMP})

CC			= @clang
CFLAGS		= -Wall -Wextra -Werror -g3# -fsanitize=address

${NAME}: ${OBJS}
	${CC} ${CFLAGS} ${OBJS} ${LINUX} -o ${NAME}
	@echo "\033[1;92m\t>>> Compilation OK <<<\033[0m"

${OBJS_PATH}%.o: %.c
			@mkdir -p ${OBJS_PATH}
			${CC} -c ${CFLAGS} -o $@ $< ${INCS}

all:	${NAME}

clean:
	@rm -rf objs

fclean:	clean
	@rm -rf ${NAME} pipex
	@echo "\033[1;91m\t>>> Project FClean <<<\033[0m"

re:			fclean all

exec: re clean
#flags valgrind : valgrind --leak-check=full --show-leak-kinds=all 


.PHONY:		all clean fclean re exec
