#ifndef FUNCTIONS_H
# define FUNCTIONS_H

# include "header.h"

typedef struct s_arg
{
	int		fds[2];
	char	*input;
	char	*cmd1;
	char	*cmd2;
	char	*output;
}	t_arg;

size_t	lib_strlen(char *str);
char	*spe_strdup(char *s1, int spe);
char	*lib_strjoin(char *s1, char *s2);
char	*copy_arg(char *argv);

int		lib_strncmp(const char *s1, const char *s2, size_t n);
char	**lib_split(char *s, char c);
void	lib_putstr(char *s);

void	free_struct(t_arg arg);
void	quit_error(char *text, t_arg arg);
t_arg	cast_arg(char **argv);

void	access_exec(char **lst_cmd, char **env, t_arg arg, int i);
void	in_proc(int file, t_arg arg, char **env);
void	out_proc(int file, t_arg arg, char **env);
void	my_pipe(t_arg arg, char **env);

char	*get_env(char *var, char **env);
char	**my_env(char *var, char **env);

void	*free_it(char **spt);
void	free_success(t_arg arg);
void	free_faillure(t_arg arg);
void	free_access(char **arg1, char **arg2, t_arg	arg);

#endif
