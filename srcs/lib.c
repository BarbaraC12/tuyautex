#include "../header.h"

size_t	lib_strlen(char *str)
{
	size_t	len;

	len = 0;
	while (str[len])
		len++;
	return (len);
}

char	*spe_strdup(char *s1, int spe)
{
	char	*str;
	int		i;

	i = 0;
	str = (char *)malloc(sizeof(char) * lib_strlen(s1) + 1 - spe);
	if (!str)
		return (NULL);
	while (s1[spe])
	{
		str[i] = (char)s1[spe];
		i++;
		spe++;
	}
	str[i] = '\0';
	return (str);
}

char	*lib_strjoin(char *s1, char *s2)
{
	char	*str;
	int		i;
	int		j;
	int		len;

	i = 0;
	j = 0;
	if (!s1 || !s2)
		return (NULL);
	len = lib_strlen(s1) + lib_strlen(s2);
	str = (char *)malloc(sizeof(char) * (len + 1));
	if (str == NULL)
		return (NULL);
	while (s1[i] != '\0')
	{
		str[i] = s1[i];
		i++;
	}
	while (s2[j] != '\0')
	{
		str[j + i] = s2[j];
		j++;
	}
	str[i + j] = '\0';
	return (str);
}

char	*copy_arg(char *argv)
{
	char	*arg;
	int		i;

	i = 0;
	arg = (char *)malloc(sizeof(char) * lib_strlen(argv) + 1);
	if (!arg)
		return (NULL);
	while (argv[i])
	{
		arg[i] = argv[i];
		i++;
	}
	arg[i] = '\0';
	return (arg);
}
