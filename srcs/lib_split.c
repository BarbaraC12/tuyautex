#include "../header.h"

static int	lib_countw(const char *str, char c)
{
	int	word;

	word = 0;
	if (*str != c && *str)
	{
		str++;
		word++;
	}
	while (*str)
	{
		while (*str == c)
		{
			str++;
			if (*str != c && *str)
				word++;
		}
		str++;
	}
	return (word);
}

static int	lib_lenw(const char *str, char c)
{
	int	count;

	count = 0;
	while (*str != c && *str)
	{
		count++;
		str++;
	}
	return (count);
}

char	**lib_split(char *s, char c)
{
	int		j;
	int		i;
	char	**spt;

	j = 0;
	spt = malloc(sizeof(char *) * (lib_countw(s, c) + 1));
	if (!s || !spt)
		return (NULL);
	while (*s)
	{
		while (*s == c && *s)
			s++;
		if (*s != c && *s)
		{
			i = 0;
			spt[j] = malloc(sizeof(char) * (lib_lenw(s, c) + 1));
			if (!spt[j])
				return (free_it(spt));
			while (*s && *s != c)
				spt[j][i++] = (char)*s++;
			spt[j++][i] = '\0';
		}
	}
	spt[j] = NULL;
	return (spt);
}

void	lib_putstr(char *s)
{
	int	i;

	i = 0;
	if (!s)
		return ;
	while (s[i])
	{
		write(1, &s[i], 1);
		i++;
	}
}
