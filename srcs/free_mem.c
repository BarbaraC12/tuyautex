#include "../header.h"

void	*free_it(char **spt)
{
	int		i;

	i = 0;
	while (spt[i])
	{
		free(spt[i++]);
	}
	free(spt);
	return (NULL);
}

void	free_success(t_arg arg)
{
	int	i;

	i = 0;
	if (arg.input != NULL)
		free(arg.input);
	if (arg.cmd1 != NULL)
		free(arg.cmd1);
	if (arg.cmd2 != NULL)
		free(arg.cmd2);
	if (arg.output != NULL)
		free(arg.output);
	exit(EXIT_SUCCESS);
}

void	free_faillure(t_arg arg)
{
	int	i;

	i = 0;
	if (arg.input != NULL)
		free(arg.input);
	if (arg.cmd1 != NULL)
		free(arg.cmd1);
	if (arg.cmd2 != NULL)
		free(arg.cmd2);
	if (arg.output != NULL)
		free(arg.output);
	exit(EXIT_FAILURE);
}

void	free_access(char **arg1, char **arg2, t_arg	arg)
{
	free_it(arg1);
	free_it(arg2);
	free_faillure(arg);
}
