#include "../functions.h"
#include "../header.h"

static void	close_fd(int *file, int fds1, int fds2)
{
	close(file[0]);
	close(file[1]);
	close(fds1);
	close(fds2);
}

void	access_exec(char **lst_cmd, char **env, t_arg arg, int i)
{
	char	**lst_env;
	char	*tmp;
	char	*pathname;

	i = 0;
	lst_env = my_env("PATH=", env);
	if (lst_env == NULL)
	{
		free(lst_env);
		free_it(lst_cmd);
		quit_error("./pipex: no command found (ENV)", arg);
	}
	while (lst_env[i] != NULL)
	{
		tmp = lib_strjoin(lst_env[i], "/");
		pathname = lib_strjoin(tmp, lst_cmd[0]);
		free(tmp);
		if (access(lst_env[i], X_OK) == 0)
			execve(pathname, lst_cmd, env);
		free(pathname);
		i++;
	}
	if (lst_env[i] == NULL)
		perror("./pipex: command not found");
	free_access(lst_cmd, lst_env, arg);
}

void	in_proc(int file, t_arg arg, char **env)
{
	char	**lst_cmd;
	int		i;

	i = 0;
	dup2(file, 0);
	dup2(arg.fds[STDOUT], 1);
	close(file);
	close(arg.fds[STDOUT]);
	close(arg.fds[STDIN]);
	if (lib_strlen(arg.cmd1) == 0)
		perror("./pipex: cmd1 permission denied");
	lst_cmd = lib_split(arg.cmd1, ' ');
	if (lst_cmd[0] != NULL)
	{
		if (lst_cmd[0][0] == '/')
			execve(lst_cmd[0], lst_cmd, env);
		access_exec(lst_cmd, env, arg, i);
	}
	free(lst_cmd);
}

void	out_proc(int file, t_arg arg, char **env)
{
	int		i;
	char	**lst_cmd;

	i = 0;
	dup2(file, 1);
	dup2(arg.fds[STDIN], 0);
	close(file);
	close(arg.fds[STDOUT]);
	close(arg.fds[STDIN]);
	lst_cmd = lib_split(arg.cmd2, ' ');
	if (lst_cmd[0] != NULL)
	{
		if (lst_cmd[0][0] == '/')
			execve(lst_cmd[0], lst_cmd, env);
		access_exec(lst_cmd, env, arg, i);
	}
	free(lst_cmd);
}

void	my_pipe(t_arg arg, char **env)
{
	pid_t	cpid[2];
	int		file[2];

	if (pipe(arg.fds) == -1)
		quit_error("./pipex: pipe", arg);
	file[1] = open(arg.output, O_CREAT | O_WRONLY | O_TRUNC, 0666);
	if (file[1] < 0)
		quit_error("./pipex: stdout", arg);
	cpid[0] = fork();
	if (cpid[0] < 0)
		quit_error("./pipex: fork out", arg);
	else if (cpid[0] == 0)
		out_proc(file[1], arg, env);
	file[0] = open(arg.input, O_RDONLY);
	if (file[0] < 0)
		quit_error("./pipex: stdin", arg);
	cpid[1] = fork();
	if (cpid[1] < 0)
		quit_error("./pipex: fork in", arg);
	else if (cpid[1] == 0)
		in_proc(file[0], arg, env);
	close_fd(file, arg.fds[STDIN], arg.fds[STDOUT]);
	waitpid(cpid[0], arg.fds, 0);
	waitpid(cpid[1], arg.fds, 0);
}
