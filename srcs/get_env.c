#include "../header.h"

int	lib_strncmp(const char *s1, const char *s2, size_t n)
{
	size_t	i;

	i = 0;
	if (n == 0)
		return (0);
	while (i + 1 < n && (s1[i] == s2[i] && (s1[i]) && (s2[i])))
		i++;
	return ((unsigned char)s1[i] - (unsigned char)s2[i]);
}

char	*get_env(char *var, char **env)
{
	int		i;
	char	*path_var;
	char	*tmp;

	i = 0;
	while (env[i] != NULL)
	{
		if (lib_strncmp(env[i], var, lib_strlen(var)) == 0)
		{
			tmp = copy_arg(env[i]);
			path_var = spe_strdup(tmp, lib_strlen(var));
			free(tmp);
			return (path_var);
		}
		i++;
	}
	return (NULL);
}

char	**my_env(char *var, char **env)
{
	char	*path_env;
	char	**lst_env;

	path_env = get_env(var, env);
	if (path_env == NULL)
	{
		free(path_env);
		return (NULL);
	}
	lst_env = lib_split(path_env, ':');
	free(path_env);
	return (lst_env);
}
