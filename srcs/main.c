#include "../header.h"

void	quit_error(char *text, t_arg arg)
{
	perror(text);
	free_faillure(arg);
}

t_arg	cast_arg(char **argv)
{
	t_arg	arg;

	arg.input = copy_arg(argv[1]);
	arg.cmd1 = copy_arg(argv[2]);
	arg.cmd2 = copy_arg(argv[3]);
	arg.output = copy_arg(argv[4]);
	if (lib_strlen(arg.cmd2) == 0)
	{
		lib_putstr("./pipex: cmd2 permission denied: Invalid argument\n");
		free_faillure(arg);
	}
	return (arg);
}

int	main(int argc, char **argv, char **env)
{
	t_arg	arg;

	if (argc != 5)
	{
		lib_putstr("Error\nUsage : ./pipex file1 cmd1 cmd2 file2\n");
		exit(EXIT_FAILURE);
	}
	else
	{
		arg = cast_arg(argv);
		my_pipe(arg, env);
		free_success(arg);
	}
	return (0);
}
