echo "\033[0;37m=======================================================\033[m "
echo "\033[0;37m===\033[m           \033[1;33mPPPP  III PPPP  EEEE X   X\033[m            \033[0;37m===\033[m "
echo "\033[0;37m===\033[m           \033[1;33mP   P  I  P   P E     X X \033[m            \033[0;37m===\033[m " 
echo "\033[0;37m===\033[m           \033[1;33mPPPP   I  PPPP  EEE    X  \033[m            \033[0;37m===\033[m "
echo "\033[0;37m===\033[m           \033[1;33mP      I  P     E     X X \033[m            \033[0;37m===\033[m "
echo "\033[0;37m===\033[m           \033[1;33mP     III P     EEEE X   X\033[m            \033[0;37m===\033[m "
echo "\033[0;37m=======================================================\033[m "
echo "\033[0;37m===\033[m                \033[1;31mBEGIN CRASH TEST\033[m                 \033[0;37m===\033[m "
echo "\033[0;37m=======================================================\033[m "

echo "\033[0;37m===\033[m  \033[1;33mTEST 1\033[m:\tToo much arguments"
./pipex infile cmd1 cmd2 outfile etcs
echo
sleep 5

echo "\033[0;37m===\033[m  \033[1;33mTEST 2\033[m:\tNo enought arguments"
./pipex infile cmd1 cmd2
echo
sleep 5

echo "\033[0;37m===\033[m  \033[1;33mTEST 3\033[m:\tValgrind env -i"
env -i valgrind --leak-check=full --show-leak-kinds=all --track-fds=yes --trace-children=yes ./pipex Makefile cat ls /dev/stdout
echo
sleep 5

echo "\033[0;37m===\033[m  \033[1;33mTEST 4\033[m:\tValgrind test leaks "
valgrind --leak-check=full --show-leak-kinds=all --track-fds=yes --trace-children=yes ./pipex Makefile cat ls /dev/stdout
echo
sleep 5

echo "\033[0;37m===\033[m  \033[1;33mTEST 5\033[m:\tValgrind 1st pipe"
valgrind --leak-check=full --show-leak-kinds=all --track-fds=yes --trace-children=yes ./pipex Makefile ploup ls /dev/stdout
echo
sleep 5

echo "\033[0;37m===\033[m  \033[1;33mTEST 6\033[m:\tValgrind 2nd pipe"
valgrind --leak-check=full --show-leak-kinds=all --track-fds=yes --trace-children=yes ./pipex Makefile "cat" "prout" /dev/stdout
echo
sleep 5

echo "\033[0;37m===\033[m  \033[1;33mTEST 7\033[m:\tNo right 1st"
echo "J'ai faim " > test.txt
chmod 000 test.txt
./pipex test.txt "cat" "wc -c" /dev/stdout
echo
sleep 5

echo "\033[0;37m===\033[m  \033[1;33mTEST 8\033[m:\tNo right 2nd"
./pipex Makefile "cat" "wc -c" test.txt
cat test.txt
echo
sleep 5

echo "\033[0;37m===\033[m  \033[1;33mTEST 9\033[m:\tInexistant file1"
./pipex tst.txt ls wc /dev/stdout
echo
sleep 5

echo "\033[0;37m===\033[m  \033[1;33mTEST 10\033[m:\tWrong cmd1"
./pipex Makefile "prout" cat /dev/stdout
echo
sleep 5

echo "\033[0;37m===\033[m  \033[1;33mTEST 11\033[m:\tWrong cmd2"
./pipex Makefile ls "prout" /dev/stdout
echo
sleep 5

echo "\033[0;37m===\033[m  \033[1;33mTEST 12\033[m:\tFile1 is folder"
mkdir folder.txt
./pipex folder.txt cat "wc - l" /dev/stdout
echo
sleep 5

echo "\033[0;37m===\033[m  \033[1;33mTEST 13\033[m:\tFilepath 1 empty"
./pipex "" ls "cat -e" /dev/stdout
echo
sleep 5

echo "\033[0;37m===\033[m  \033[1;33mTEST 14\033[m:\tFilepath 2 empty"
./pipex Makefile cat "wc -c" ""
echo
sleep 5

echo "\033[0;37m===\033[m  \033[1;33mTEST 15\033[m:\tCmd 1 empty"
./pipex Makefile "" "cat -e" /dev/stdout
echo
sleep 5

echo "\033[0;37m===\033[m  \033[1;33mTEST 16\033[m:\tCmd 2 empty"
./pipex Makefile cat "" "/dev/stdout"
echo
sleep 5

echo "\033[0;37m===\033[m  \033[1;33mTEST 17\033[m:\tPath empty"
export OP=$PATH PATH=""
./pipex Makefile cat "wc -c" outfile2
export PATH=$OP ; unset OP
ls
echo
sleep 5

echo "\033[0;37m=======================================================\033[m "
echo "\033[0;37m===\033[m   \033[1;31mEND OF CRASH TEST\033[m        \033[1;32mBEGIN COMMONS TEST\033[m   \033[0;37m===\033[m "
echo "\033[0;37m=======================================================\033[m "


echo "\033[0;37m===\033[m  \033[1;33mTEST 1\033[m:\t./pipex Makefile echo wc -c oufile1"
./pipex Makefile cat "wc -c" outfile1
cat outfile1
echo
sleep 5

echo "\033[0;37m===\033[m  \033[1;33mTEST 2\033[m:\t./pipex /dev/stdin cat ls /dev/stdout"
./pipex /dev/stdin cat ls /dev/stdout
echo
sleep 5

echo "Test compil whit this flag:"
echo "\033[1;37m -fsanitize=address -fsanitize=undefined -fsanitize=leak -g3\033[m "
